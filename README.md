# Debug primitives for blender

> A simple module for drawing debug primitives into the blender viewport.

###

## Installation

### Addons

Copy paste [debug.py](debug.py) into your addon folder.

### Blender

Copy paste [debug.py](debug.py) into `blender_root\blender_version\python\lib\`

## Basic usage

Draw a simple sinus:

~~~python
from math import sin
import debug

for x in range(-30,30):
    x = x/10
    debug.draw_point(
        location=(x,sin(x),0),
        duration=2,
        color=(1,0,0,1))
~~~

result:

![sin](docs/sin_example.png)

## Contributing

1. Fork it (<https://github.com/yourname/yourproject/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
